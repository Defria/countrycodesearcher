import { Component, OnInit } from "@angular/core";

import { Item } from "./item";
import { ItemService } from "./item.service";


import { SearchBar } from "tns-core-modules/ui/search-bar";
import { isIOS } from "tns-core-modules/platform";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { isAndroid } from "tns-core-modules/platform";

@Component({
    selector: "ns-items",
    templateUrl: "./items.component.html"
})
export class ItemsComponent implements OnInit {
    items: Array<Item>;
    news: Array<Item>;

    constructor(private itemService: ItemService) {
        this.actionAndroid = isAndroid;
        this.items = itemService.getItems();
        this.news = this.items.filter((e) => {
            return e.country;
        });

        // if (isIOS) {
        //     var keyboard = IQKeyboardManager.sharedManager();
        //     keyboard.shouldResignOnTouchOutside = true;
        // }
    }


    searching = false;


    actionAndroid;


    public onSubmit(args) {
        let searchBar = <SearchBar>args.object;
        this.onSearch(searchBar.text ? searchBar.text.toLowerCase() : "");
        searchBar.dismissSoftInput();
    }

    onSearch(searchValue) {
        if (searchValue !== "") {
            this.news = this.items.filter((e) => {
                return (e.code) &&
                    (e.country.toLowerCase().includes(searchValue) || e.code.toLowerCase().includes(searchValue));
            });
        }
    }

    public onClear(args) {
        let searchBar = <SearchBar>args.object;
        searchBar.text = "";
        searchBar.hint = "Search country code...";
        this.items.forEach(item => {
            this.news.push(item);
        });
        searchBar.dismissSoftInput();
        this.searching = false;
    }

    public onTextChange(args) {
        let searchBar = <SearchBar>args.object;
        this.onSearch(searchBar.text ? searchBar.text.toLowerCase() : "");
    }


    ngOnInit(): void {
        this.items = this.itemService.getItems();
    }
}
